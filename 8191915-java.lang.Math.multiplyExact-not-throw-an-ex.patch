From eeb0317f3582ae74dd7d42d149fdc457cd01d835 Mon Sep 17 00:00:00 2001
Date: Thu, 26 Dec 2019 20:04:58 +0000
Subject: [PATCH] 8191915: java.lang.Math.multiplyExact not throw an exception
 for certain values

Summary: C2: java.lang.Math.multiplyExact not throw an exception for certain values
LLT: hotspot/test/compiler/intrinsics/mathexact/LongMulOverflowTest.java
Bug url: https://bugs.openjdk.java.net/browse/JDK-8191915
---
 hotspot/src/share/vm/opto/mathexactnode.cpp        | 42 +++++++++------
 hotspot/src/share/vm/opto/mathexactnode.hpp        |  4 +-
 .../intrinsics/mathexact/LongMulOverflowTest.java  | 61 ++++++++++++++++++++++
 3 files changed, 90 insertions(+), 17 deletions(-)
 create mode 100644 hotspot/test/compiler/intrinsics/mathexact/LongMulOverflowTest.java

diff --git a/hotspot/src/share/vm/opto/mathexactnode.cpp b/hotspot/src/share/vm/opto/mathexactnode.cpp
index 00466ad3d5..661cc745bf 100644
--- a/hotspot/src/share/vm/opto/mathexactnode.cpp
+++ b/hotspot/src/share/vm/opto/mathexactnode.cpp
@@ -1,5 +1,5 @@
 /*
- * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
+ * Copyright (c) 2013, 2018, Oracle and/or its affiliates. All rights reserved.
  * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
  *
  * This code is free software; you can redistribute it and/or modify it
@@ -117,23 +117,33 @@ bool OverflowSubLNode::will_overflow(jlong v1, jlong v2) const {
   return SubHelper<OverflowSubLNode>::will_overflow(v1, v2);
 }
 
-bool OverflowMulLNode::will_overflow(jlong val1, jlong val2) const {
-    jlong result = val1 * val2;
-    jlong ax = (val1 < 0 ? -val1 : val1);
-    jlong ay = (val2 < 0 ? -val2 : val2);
-
-    bool overflow = false;
-    if ((ax | ay) & CONST64(0xFFFFFFFF00000000)) {
-      // potential overflow if any bit in upper 32 bits are set
-      if ((val1 == min_jlong && val2 == -1) || (val2 == min_jlong && val1 == -1)) {
-        // -1 * Long.MIN_VALUE will overflow
-        overflow = true;
-      } else if (val2 != 0 && (result / val2 != val1)) {
-        overflow = true;
-      }
+bool OverflowMulLNode::is_overflow(jlong val1, jlong val2) {
+    // x * { 0, 1 } will never overflow. Even for x = min_jlong
+    if (val1 == 0 || val2 == 0 || val1 == 1 || val2 == 1) {
+      return false;
+    }
+
+    // x * min_jlong for x not in { 0, 1 } overflows
+    // even -1 as -1 * min_jlong is an overflow
+    if (val1 == min_jlong || val2 == min_jlong) {
+      return true;
     }
 
-    return overflow;
+    // if (x * y) / y == x there is no overflow
+    //
+    // the multiplication here is done as unsigned to avoid undefined behaviour which
+    // can be used by the compiler to assume that the check further down (result / val2 != val1)
+    // is always false and breaks the overflow check
+    julong v1 = (julong) val1;
+    julong v2 = (julong) val2;
+    julong tmp = v1 * v2;
+    jlong result = (jlong) tmp;
+
+    if (result / val2 != val1) {
+      return true;
+    }
+
+    return false;
 }
 
 bool OverflowAddINode::can_overflow(const Type* t1, const Type* t2) const {
diff --git a/hotspot/src/share/vm/opto/mathexactnode.hpp b/hotspot/src/share/vm/opto/mathexactnode.hpp
index 3e037cf568..0a59ebd96e 100644
--- a/hotspot/src/share/vm/opto/mathexactnode.hpp
+++ b/hotspot/src/share/vm/opto/mathexactnode.hpp
@@ -129,8 +129,10 @@ public:
   OverflowMulLNode(Node* in1, Node* in2) : OverflowLNode(in1, in2) {}
   virtual int Opcode() const;
 
-  virtual bool will_overflow(jlong v1, jlong v2) const;
+  virtual bool will_overflow(jlong v1, jlong v2) const { return is_overflow(v1, v2); }
   virtual bool can_overflow(const Type* t1, const Type* t2) const;
+
+  static bool is_overflow(jlong v1, jlong v2);
 };
 
 #endif
diff --git a/hotspot/test/compiler/intrinsics/mathexact/LongMulOverflowTest.java b/hotspot/test/compiler/intrinsics/mathexact/LongMulOverflowTest.java
new file mode 100644
index 0000000000..69bd8f1579
--- /dev/null
+++ b/hotspot/test/compiler/intrinsics/mathexact/LongMulOverflowTest.java
@@ -0,0 +1,61 @@
+/*
+ * Copyright (c) 2018, Oracle and/or its affiliates. All rights reserved.
+ * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
+ *
+ * This code is free software; you can redistribute it and/or modify it
+ * under the terms of the GNU General Public License version 2 only, as
+ * published by the Free Software Foundation.
+ *
+ * This code is distributed in the hope that it will be useful, but WITHOUT
+ * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
+ * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
+ * version 2 for more details (a copy is included in the LICENSE file that
+ * accompanied this code).
+ *
+ * You should have received a copy of the GNU General Public License version
+ * 2 along with this work; if not, write to the Free Software Foundation,
+ * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
+ *
+ * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
+ * or visit www.oracle.com if you need additional information or have any
+ * questions.
+ */
+
+/*
+ * @test
+ * @bug 8191915
+ * @summary Regression test for multiplyExact intrinsic
+ * @compile AddExactICondTest.java
+ * @run main/othervm -Xcomp -XX:-TieredCompilation compiler.intrinsics.mathexact.LongMulOverflowTest
+ */
+
+package compiler.intrinsics.mathexact;
+
+public class LongMulOverflowTest {
+    public static void main(String[] args) {
+        LongMulOverflowTest test = new LongMulOverflowTest();
+        for (int i = 0; i < 10; ++i) {
+            try {
+                test.runTest();
+                throw new RuntimeException("Error, runTest() did not overflow!");
+            } catch (ArithmeticException e) {
+                // success
+            }
+
+            try {
+                test.runTestOverflow();
+                throw new RuntimeException("Error, runTestOverflow() did not overflow!");
+            } catch (ArithmeticException e) {
+                // success
+            }
+        }
+    }
+
+    public void runTest() {
+        java.lang.Math.multiplyExact(Long.MIN_VALUE, 7);
+    }
+
+    public void runTestOverflow() {
+      java.lang.Math.multiplyExact((Long.MAX_VALUE / 2) + 1, 2);
+    }
+}
-- 
2.12.3

